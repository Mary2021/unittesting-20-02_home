const ValidateIsNumberInputs = require('./validate_is_number_inputs');

function sum(a, b) {
  ValidateIsNumberInputs(a, b);
  return a + b;
}

module.exports = sum;
