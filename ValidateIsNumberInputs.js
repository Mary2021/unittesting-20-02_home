function ValidateIsNumberInputs(a, b) {
  if (Number.isNaN(a) || Number.isNaN(b)) {
    throw new Error('invalid input');
  }
  if (typeof a === 'string' || typeof b === 'string') {
    throw new Error('input need to be number');
  }
}

module.exports = ValidateIsNumberInputs;
