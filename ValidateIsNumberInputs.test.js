const ValidateIsNumberInputs = require('./ValidateIsNumberInputs');

test('check valid input (error object param 2)', () => {
  expect(() => {
    ValidateIsNumberInputs(1, new Error('fail'));
  }).toThrow('invalid input');
});

test('check valid input (error object param 1)', () => {
  expect(() => {
    ValidateIsNumberInputs(new Error('fail'), 1);
  }).toThrow('invalid input');
});

test('check valid input (string param 2)', () => {
  expect(() => {
    ValidateIsNumberInputs(1, '4');
  }).toThrow('input need to be number');
});

test('check valid input (string param 1)', () => {
  expect(() => {
    ValidateIsNumberInputs('4', 1);
  }).toThrow('input need to be number');
});

test('function as param', () => {
  expect(() => {
    ValidateIsNumberInputs(1, () => 5);
  }).toThrow('invalid input');
});
